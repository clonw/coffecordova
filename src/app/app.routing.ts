import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';


// Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { TakecoffeeComponent} from './components/takecoffee/takecoffee.component';
import { RegisterComponent } from './components/register/register.component';
import { RondasComponent } from './components/rondas/rondas.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: '', component: HomeComponent},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'login', component: LoginComponent},
    {path: 'registro', component: RegisterComponent},
    {path: 'tomarcafe', component: TakecoffeeComponent},
    {path: 'historico', component: RondasComponent},
    {path: '**', component: HomeComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

