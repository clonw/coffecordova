import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing, appRoutingProviders} from './app.routing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';

// Mis componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { TakecoffeeComponent } from './components/takecoffee/takecoffee.component';
import { RegisterComponent } from './components/register/register.component';
import { RondasComponent } from './components/rondas/rondas.component';
import { SearchPipe} from './pipes/search.pipe';
import { RoundDetailComponent } from './components/round-detail/round-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    TakecoffeeComponent,
    RegisterComponent,
    RondasComponent,
    SearchPipe,
    RoundDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    BrowserAnimationsModule,
    HttpModule
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
