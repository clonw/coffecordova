import { Injectable, Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'search'
})

@Injectable()
export class SearchPipe implements PipeTransform {
    transform(companies: any[], path: string[], order: number): any[] {

        // Check if is not null
        if (!companies || !path || !order) {
            return companies;
        }
        return companies.sort((a: any, b: any) => {
          // We go for each property followed by path
          path.forEach(property => {
            a = a[property];
            b = b[property];
          });

          // Order * (-1): We change our order
          return a > b ? order : order * (- 1);
        });
      }
}

