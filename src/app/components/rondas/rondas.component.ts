import { Component, OnInit } from '@angular/core';
import { RondaService } from '../../services/ronda.service';
import { UserService} from '../../services/user.service';
import { Ronda } from '../../models/ronda';

@Component({
  selector: 'app-rondas',
  templateUrl: './rondas.component.html',
  styleUrls: ['./rondas.component.css'],
  providers: [ RondaService, UserService ]
})
export class RondasComponent implements OnInit {

  public title: String;
  public rounds: any[];
  public status: String;
  public sortType: any;
  path: string[] = ['date'];
  order: number; // 1 asc, -1 desc;
  public selectedRound: any;
  public amIAdministrator: boolean;
  public deletedRound: any;

  constructor(
    private _rondaService: RondaService,
    private _userService: UserService
  ) {
    this.title = 'Histórico';
    this.rounds = new Array<any>();
    this.order = -1;
    this.amIAdministrator = false;
   }

  ngOnInit() {
    this.getRounds();
    const identity = this._userService.getIdentity();
    console.log( JSON.stringify(identity));
    if (identity) {
      const role = identity.role;
      if (role === 'ROLE_ADMIN') {
        this.amIAdministrator = true;
      }
    }
  }

  selectRound( id: string) {
    this.selectedRound = id;
  }

  getRounds() {
    this._rondaService.getAllrounds().subscribe(
      response => {
        if ( !response.rounds) {
          alert('No se han recuperado rondas');
        } else {
          this.rounds = response.rounds;

          for ( let i = 0; i < this.rounds.length; i++) {
            const consumed = this.rounds[i].diner.length;

            this.rounds[ i].consumed = consumed;
          }

        }
      },
      error => {
        const errorMessage = <any> error;

        if ( errorMessage != null) {
            const body = JSON.stringify(error._body);
            this.status = 'error';
        }
      }
    );
  }

  deleteRound(id: string) {
    this._rondaService.deleteRound(id).subscribe(
      response => {
        if ( !response.round) {
          alert('No se ha eliminado la ronda');
        } else {
          this.deletedRound = response.round;
          const consumed = this.deletedRound.diner.length;
          this.deletedRound.consumed = consumed;
          console.log('Se elimina esta ronda');
          console.log(JSON.stringify(this.deletedRound));
        }
      }, error => {
        const errorMessage = <any> error;

        if ( errorMessage != null) {
            const body = JSON.stringify(error._body);
            this.status = 'error';
        }
      }
    );
    this.getRounds();
  }
}
