import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { RondaService } from '../../services/ronda.service';
import { User } from '../../models/user';
import { Ronda } from '../../models/ronda';

@Component({
  selector: 'app-takecoffee',
  templateUrl: './takecoffee.component.html',
  // styleUrls: [ './takecoffee.component.css' ],
  providers: [ UserService, RondaService ]
})
export class TakecoffeeComponent implements OnInit {

  public title: String;
  public status: string;
  public coffeeTakers: any[];
  public selectedCoffeeTakers: any[];
  public ratios: Map<string, number>;
  public date: Date;
  public ronda: Ronda;

  constructor(
    private _userService: UserService,
    private _rondaService: RondaService
  ) {
    this.title = 'Tomemos café';
    this.selectedCoffeeTakers = new Array<any>();
    this.ratios = new Map<string, number>();
    this.date = new Date();
    this.ronda = new Ronda('', [''], new Date(), '');
  }

  ngOnInit() {
    this.cafeteros();
  }

  cafeteros() {
    this._userService.getCafeteros().subscribe(
      response => {
        if ( !response.users) {
          alert('No se han recuperado cafeteros');
        } else {
          this.coffeeTakers = response.users;

          for (let i = 0; i < this.coffeeTakers.length; i++) {
            let ratio = 0;
            ratio = this.coffeeTakers[i].paid / this.coffeeTakers[i].consumed;
            this.coffeeTakers[i].ratio = ratio;
          }
        }
      },
      error => {
        const errorMessage = <any> error;

        if ( errorMessage != null) {
            const body = JSON.stringify(error._body);
            this.status = 'error';
        }
      }
    );
  }

  selectCoffee(index: number) {
    if (!this.selectedCoffeeTakers.includes(this.coffeeTakers[ index])) {
      this.selectedCoffeeTakers.push( this.coffeeTakers[ index]);
      this.selectedCoffeeTakers.sort(( t1, t2) => {
        if (t1.ratio > t2.ratio) {
            return 1;
        }
        if (t1.ratio < t2.ratio) {
            return -1;
        }
        return 0;
      });
    }
  }

  deleteSekected( index: number) {
    if (index > -1) {
      this.selectedCoffeeTakers.splice(index, 1);
    }
  }

  pagarRonda( id: string) {
    const diners: [string] = [''];

    for (let i = 0; i < this.selectedCoffeeTakers.length; i++) {
      diners.push( this.selectedCoffeeTakers[i]._id);
    }

    this.ronda = new Ronda( id, diners, this.date, 'Arquitectura');

    this._rondaService.payRound( this.ronda).subscribe(
      response => {
          if (!response.ronda) {
              this.status = 'error';
          } else {
              this.status = 'success';
              this.ronda = response.ronda;
          }
      },
      error => {
          const errorMessage = <any>error;
          if (errorMessage != null) {
              this.status = 'error';
          }
      }
    );

    this.selectedCoffeeTakers = new Array<any>();

    this.cafeteros();

  }

  parseDate(dateString: string): Date {
    if (dateString) {
        return new Date(dateString);
    } else {
        return null;
    }
  }
}
