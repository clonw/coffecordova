import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakecoffeeComponent } from './takecoffee.component';

describe('TakecoffeeComponent', () => {
  let component: TakecoffeeComponent;
  let fixture: ComponentFixture<TakecoffeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakecoffeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakecoffeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
