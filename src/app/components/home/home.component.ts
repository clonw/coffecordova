import { Component, OnInit } from '@angular/core';
import { fadeIn} from '../animation';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'home',
  templateUrl: './home.component.html',
  animations: [fadeIn]
})
export class HomeComponent implements OnInit {
  title = 'Coffe Time';

  ngOnInit() {
    console.log('home.component cargado');
  }
}
