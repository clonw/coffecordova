import { Component, OnInit, Input } from '@angular/core';
import { RondaService } from '../../services/ronda.service';

@Component({
  selector: 'app-round-detail',
  templateUrl: './round-detail.component.html',
  styleUrls: ['./round-detail.component.css'],
  providers: [ RondaService ]
})
export class RoundDetailComponent implements OnInit {
  @Input() id: string;
  @Input() public selectedRound: any;
  public title: String;
  public status: String;

  constructor(
    private _rondaService: RondaService
  ) {
    this.title = 'Detalle Ronda';
   }

  ngOnInit() {
    if ( this.id) {
      this.getRound( this.id);
    }
  }

  getRound( id: string) {
    this._rondaService.getRound(id).subscribe(
      response => {
        if ( !response.round) {
          alert('No se han recuperado rondas');
        } else {
          this.selectedRound = response.round;
          const consumed = this.selectedRound.diner.length;
          this.selectedRound.consumed = consumed;
        }
      }, error => {
        const errorMessage = <any> error;

        if ( errorMessage != null) {
            const body = JSON.stringify(error._body);
            this.status = 'error';
        }
      }
    );
  }
}
