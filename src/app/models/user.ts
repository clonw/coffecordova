export class User {
    constructor(
        public _id: string,
        public name: string,
        public surname: string,
        public email: string,
        public password: string,
        public role: string,
        public image: string,
        public consumed: number,
        public paid: number,
        public project: string,
        public rounds: [string]
    ) {}
}

