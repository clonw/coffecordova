export class Ronda {
    constructor(
        public payer: string,
        public diner: [string],
        public date: Date,
        public project: string
    ) {}
}
