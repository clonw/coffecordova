import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
/** esto nos va a dar un método que nos permite recoger los valores que da una peticion ajax */
import 'rxjs/add/operator/map';
import { Observable} from 'rxjs/Observable';
import { GLOBAL} from './global';
import { UserService } from './user.service';

/**Para poder inyectar el servicio dentro de componente y dentro de diferentes sitios */
@Injectable()
export class RondaService {
    public url: string;

    constructor(
        private _http: Http,
        private _userService: UserService
    ) {
        this.url = GLOBAL.url;
    }

    /**
     * Paga una ronda
     * @param round --> Ronda a pagar
     */
    payRound(round) {
        const params = JSON.stringify(round);
        const headers = new Headers({
            'Content-Type' : 'application/json',
            // 'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : this._userService.getToken()
        });

        return this._http.put(this.url + 'payround', params, {headers : headers})
                        .map(res => res.json());
    }

    getAllrounds() {
        const headers = new Headers({
            'Content-Type' : 'application/json',
            // 'Content-Type' : 'application/x-www-form-urlencoded',
            'Authorization' : this._userService.getToken()
        });

        return this._http.get(this.url + 'rounds', {headers : headers})
                        .map(res => res.json());
    }

    getRound( id: string) {
        const headers = new Headers({
            'Content-Type' : 'application/json',
            'Authorization' : this._userService.getToken()
        });

        return this._http.get(this.url + 'round/' + id, {headers : headers})
                        .map(res => res.json());
    }

    deleteRound( id: string) {
        const headers = new Headers({
            'Content-Type' : 'application/json',
            'Authorization' : this._userService.getToken()
        });

        return this._http.delete(this.url + 'round/' + id, {headers : headers})
                        .map(res => res.json());
    }
}
